using Ivannuari;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ArPage : Page
{
    [SerializeField] private Button b_home;
    [SerializeField] private TargetMarker[] targets;

    [SerializeField] private Image image_price;
    [SerializeField] private GameObject panelDeskripsi;
    [SerializeField] private TMP_Text label_detail;

    private void Start()
    {
        b_home.onClick.AddListener(() => ChangeScene("Main Menu"));

        panelDeskripsi.SetActive(false);
        image_price.sprite = null;
    }

    public void OnDetectTarget(string nama)
    {
        TargetMarker marker = Array.Find(targets, t => t.nama == nama);

        image_price.sprite = marker.harga;
        panelDeskripsi.SetActive(true);
        label_detail.text = marker.detail;
    }

    public void OnLostTarget()
    {
        panelDeskripsi.SetActive(false);
        image_price.sprite = null;
    }
}




[System.Serializable]
public class TargetMarker
{
    public string nama;
    public Sprite harga;
    [TextArea(10 , 20)] public string detail;
}
