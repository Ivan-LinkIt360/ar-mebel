using Ivannuari;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubProdukPage : Page
{
    [SerializeField] private Button b_back;

    [SerializeField] private Image image_produk;
    [SerializeField] private Sprite[] allProduks;

    [SerializeField] private Button[] allContent;

    private int activePage = 0;

    private void Start()
    {
        b_back.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Produk));
    }

    public override void SetNomor(int nomor)
    {
        activePage = nomor;
        image_produk.sprite = allProduks[activePage];

        foreach (var item in allContent)
        {
            item.gameObject.SetActive(false);
        }

        if(nomor == 0)
        {
            allContent[0].gameObject.SetActive(true);
            allContent[1].gameObject.SetActive(true);
        }
        if (nomor == 1)
        {
            allContent[2].gameObject.SetActive(true);
            allContent[3].gameObject.SetActive(true);
        }
        if (nomor == 2)
        {
            allContent[4].gameObject.SetActive(true);
            allContent[5].gameObject.SetActive(true);
        }
    }

    public void SetButton(string produk)
    {
        ChangeScene("AR Scene");
    }
}
