using Ivannuari;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCanvasManager : CanvasManager
{
    public int subProdukSelected = 0;
    private void OnEnable()
    {
        GameManager.Instance.OnStateChange += Instance_OnStateChange;
    }
    private void OnDisable()
    {
        GameManager.Instance.OnStateChange -= Instance_OnStateChange;
    }

    private void Instance_OnStateChange(GameState newState)
    {
        switch (newState)
        {
            case GameState.Menu:
                SetPage(PageName.Menu);
                break;
            case GameState.Petunjuk:
                SetPage(PageName.Petunjuk);
                break;
            case GameState.Profil:
                SetPage(PageName.Profil);
                break;
            case GameState.Produk:
                SetPage(PageName.Produk);
                break;
            case GameState.SubProduk:
                SetPage(PageName.SubProduk , subProdukSelected);
                break;
        }
    }
}
