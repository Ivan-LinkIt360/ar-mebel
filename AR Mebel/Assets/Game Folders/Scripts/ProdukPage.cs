using Ivannuari;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProdukPage : Page
{
    [SerializeField] private Button b_back;

    [SerializeField] private Button b_lemari;
    [SerializeField] private Button b_meja;
    [SerializeField] private Button b_kursi;

    private MenuCanvasManager manager;

    private void Start()
    {
        manager = GetComponentInParent<MenuCanvasManager>();

        b_back.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));
        b_lemari.onClick.AddListener(() =>
        {
            manager.subProdukSelected = 0;
            GameManager.Instance.ChangeState(GameState.SubProduk);
        });
        b_meja.onClick.AddListener(() =>
        {
            manager.subProdukSelected = 1;
            GameManager.Instance.ChangeState(GameState.SubProduk);
        });
        b_kursi.onClick.AddListener(() =>
        {
            manager.subProdukSelected = 2;
            GameManager.Instance.ChangeState(GameState.SubProduk);
        });
    }
}
