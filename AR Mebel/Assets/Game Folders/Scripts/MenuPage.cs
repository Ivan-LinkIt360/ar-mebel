using Ivannuari;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuPage : Page
{
    [SerializeField] private Button b_quit;
    [SerializeField] private Button b_produk;
    [SerializeField] private Button b_petunjuk;
    [SerializeField] private Button b_profil;

    private void Start()
    {
        b_quit.onClick.AddListener(Application.Quit);
        b_produk.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Produk));
        b_profil.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Profil));
        b_petunjuk.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Petunjuk));
    }
}
